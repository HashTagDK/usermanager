package com.dk.usermanager.utils;

import com.dk.usermanager.entity.User;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class Tools {
    public String getUserNameFromSpringSecurityContext(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
