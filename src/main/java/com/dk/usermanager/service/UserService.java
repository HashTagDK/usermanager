package com.dk.usermanager.service;

import com.dk.usermanager.entity.Authority;
import com.dk.usermanager.entity.User;
import com.dk.usermanager.repository.UserRepo;
import com.dk.usermanager.utils.Tools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.tools.Tool;

@Service
public class UserService {
    @Autowired
    private UserRepo  userRepo;
    @Autowired
    private Tools tools;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Boolean createUser( User user ){
        user.setEnabled( true );
        user.setPassword( passwordEncoder.encode( user.getPassword() ));
        userRepo.save( user );
        Authority authority = new Authority();
        authority.setAuthority( "ROLE_USER");
        authority.setUser( user );
        userRepo.insertAuthority( authority );
        return true;
    }

    public Boolean deleteUser( String username ){
        if( username.equals( tools.getUserNameFromSpringSecurityContext() )){
            userRepo.deleteUserByUsernameEquals( username );
            return true;
        }else{
            return false;
        }
    }
}
