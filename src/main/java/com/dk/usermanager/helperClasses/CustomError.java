package com.dk.usermanager.helperClasses;

import com.google.common.base.Throwables;
import org.springframework.http.HttpStatus;

public class CustomError extends RuntimeException {
    private Integer code;
    private String message;
    private String causeString;
    private String stackTraceString;
    private Boolean printStackTrace;
    private HttpStatus httpStatus;

    public CustomError(Integer code, String message, Boolean printStackTrace, HttpStatus httpStatus) {
        super();
        this.code = code;
        this.message = message;
        this.printStackTrace = printStackTrace;
        this.httpStatus = httpStatus;
    }

    public CustomError(Integer code, Exception e, Boolean printStackTrace, HttpStatus httpStatus) {
        super();

        this.code = code;
        this.message = e.getMessage();
        this.causeString = Throwables.getRootCause( e ).toString();
        this.stackTraceString = Throwables.getStackTraceAsString( e );

        this.printStackTrace = printStackTrace;
        this.httpStatus = httpStatus;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCauseString() {
        return causeString;
    }

    public void setCauseString(String causeString) {
        this.causeString = causeString;
    }

    public String getStackTraceString() {
        return stackTraceString;
    }

    public void setStackTraceString(String stackTraceString) {
        this.stackTraceString = stackTraceString;
    }

    public Boolean getPrintStackTrace() {
        return printStackTrace;
    }

    public void setPrintStackTrace(Boolean printStackTrace) {
        this.printStackTrace = printStackTrace;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
