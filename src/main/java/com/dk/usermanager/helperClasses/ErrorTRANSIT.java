package com.dk.usermanager.helperClasses;

public class ErrorTRANSIT {
    private Integer code;
    private String  message;
    private String causeString;
    private String stackTraceString;
    private Boolean printStack;

    public ErrorTRANSIT(){}

    public ErrorTRANSIT(Integer code, String message, String stackTrace) {
        super();
        this.code = code;
        this.message = message;
        this.stackTraceString = stackTrace;
    }

    public ErrorTRANSIT(CustomError cE){
        this.code = cE.getCode();
        this.message = cE.getMessage();
        this.causeString = cE.getCauseString();
        this.stackTraceString = cE.getStackTraceString();
        this.printStack = cE.getPrintStackTrace();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCauseString() {
        return causeString;
    }

    public void setCauseString(String causeString) {
        this.causeString = causeString;
    }

    public String getStackTraceString() {
        return stackTraceString;
    }

    public void setStackTraceString(String stackTraceString) {
        this.stackTraceString = stackTraceString;
    }

    public Boolean getPrintStack() {
        return printStack;
    }

    public void setPrintStack(Boolean printStack) {
        this.printStack = printStack;
    }
}
