package com.dk.usermanager.repository;

import com.dk.usermanager.entity.Authority;
import com.dk.usermanager.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Transactional
public class UserRepoImpl implements UserRepoCustom{

    @PersistenceContext
    private EntityManager em;

    public User insertUser(User user) {
        em.persist( user );
        return user;
    }

    @Override
    public Authority insertAuthority(Authority authority) {
        em.persist( authority );
        return authority;
    }
}
