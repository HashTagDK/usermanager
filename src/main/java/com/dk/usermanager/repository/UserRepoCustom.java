package com.dk.usermanager.repository;

import com.dk.usermanager.entity.Authority;
import com.dk.usermanager.entity.User;

public interface UserRepoCustom {
    User insertUser(User user );
    Authority insertAuthority( Authority authority );
}
