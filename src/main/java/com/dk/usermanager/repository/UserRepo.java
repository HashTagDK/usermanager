package com.dk.usermanager.repository;

import com.dk.usermanager.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserRepo extends JpaRepository<User, Long>, UserRepoCustom {
    User findByUsername( String username );
    User  save( User user );
    Integer deleteUserByUsernameEquals( String username );
}
