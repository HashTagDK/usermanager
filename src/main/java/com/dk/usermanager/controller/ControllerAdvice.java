package com.dk.usermanager.controller;

import com.dk.usermanager.helperClasses.CustomError;
import com.dk.usermanager.helperClasses.ErrorTRANSIT;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(CustomError.class)
    public ResponseEntity<ErrorTRANSIT> error(CustomError e) {
        if (e.getHttpStatus().equals(HttpStatus.UNAUTHORIZED) && (e.getMessage() == null || e.getMessage() == "")) {
            e.setMessage("Niautoryzowany dostęp, zamknij okno następnie zaloguj się ponownie !");
        }
        return new ResponseEntity<ErrorTRANSIT>(new ErrorTRANSIT(e), e.getHttpStatus());
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorTRANSIT> otherExceptionHandler(Exception e) {
        CustomError cError = new CustomError(400, e, true, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<ErrorTRANSIT>(new ErrorTRANSIT(cError), cError.getHttpStatus());
    }

}
