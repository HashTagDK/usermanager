package com.dk.usermanager.controller;

import com.dk.usermanager.entity.Authority;
import com.dk.usermanager.entity.User;
import com.dk.usermanager.helperClasses.CustomError;
import com.dk.usermanager.repository.UserRepo;
import com.dk.usermanager.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
public class UserController {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserService userService;

    @ApiOperation(value = " hello api endpoint ")
    @GetMapping("/")
    public String helloWorld() {
        return " Hello anonymous!!!";
    }

    @GetMapping("/createUser")
    public String createUser() {
        return " Hello anonymous!!!";
    }

    @ApiOperation(value = "test post endpoint, return user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "User credencials", required = true, dataType = "User", paramType = "requestBody", defaultValue = "null")
    })
    @GetMapping("/add/{name}")
    public User testUser(@PathVariable("name") String name) {
        User u = new User();
        u.setPassword(name);
        u.setUsername(name);
        u.setEnabled(true);

        Authority authority = new Authority();
        authority.setAuthority("ROLE_USER");
        authority.setUser(u);

        userRepo.insertUser(u);
        userRepo.insertAuthority(authority);
        return u;
    }

    @GetMapping("/user")
    public String helloUser() {
        User user = new User();
        user.setUsername("Dawid");
        user.setPassword("Dawid");
        return "Hello user!" + user.getUsername();
    }

    @GetMapping("/admin")
    public String helloAdmin() {
        return "Hello Amin!";
    }
    /*--------------------- API ---------------------------------*/

    @ApiOperation(value = "Create new user, password validation is on frontend side")
    @ApiImplicitParams({@ApiImplicitParam(name = "user", value = "User credencials", required = true, dataType = "User", paramType = "requestBody", defaultValue = "null")})
    @PostMapping("/addUser")
    public ResponseEntity<Boolean> create(@RequestBody User user) {
        try {
            return new ResponseEntity<>( userService.createUser(user), HttpStatus.CREATED );
        } catch (CustomError cE) {
            throw cE;
        } catch (Exception e) {
            throw new CustomError(409, e, true, HttpStatus.CONFLICT);
        }
    }

    @ApiOperation(value = "Delete user acount by user by himslef")
    @ApiImplicitParams({@ApiImplicitParam(name = "username", value = "Username of loged user", required = true, dataType = "String", paramType = "requestParam", defaultValue = "null")})
    @DeleteMapping("/user")
    public ResponseEntity<Boolean> delete(@RequestBody String username) {
        try {
            return new ResponseEntity<>( userService.deleteUser( username ), HttpStatus.OK );
        } catch (CustomError cE) {
            throw cE;
        } catch (Exception e) {
            throw new CustomError(409, e, true, HttpStatus.CONFLICT);
        }
    }

}
